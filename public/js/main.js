jQuery(document).ready(function($){
	//final width --> this is the quick view image slider width
	//maxQuickWidth --> this is the max-width of the quick-view panel
	var sliderFinalWidth = 400,
		maxQuickWidth = 900;

	//open the quick view panel
	$('.product-trigger').on('click', function(event){
		var selectedImage = $(this).parent('.product-item').children('img'),
			slectedImageUrl = selectedImage.attr('src'),
			selectProduct=$(this).attr('href');

		$('body').addClass('overlay-layer');
		animateQuickView(selectedImage, sliderFinalWidth, maxQuickWidth, 'open',selectProduct);

		//update the visible slider image in the quick view panel
		updateQuickView(slectedImageUrl,selectProduct);
	});

	//close the quick view panel
	$('body').on('click', function(event){
		if( $(event.target).is('.flag.product-close') || $(event.target).is('body.overlay-layer')) {
			closeQuickView( sliderFinalWidth, maxQuickWidth);
		}
	});
	$(document).keyup(function(event){
		//check if user has pressed 'Esc'
    	if(event.which=='27'){
			closeQuickView( sliderFinalWidth, maxQuickWidth);
		}
	});

	//quick view slider implementation
	$('.product-quick-view').on('click', '.product-slider-navigation a', function(){
		updateSlider($(this));
	});

	//center quick-view on window resize
	$(window).on('resize', function(){
		if($('.product-quick-view').hasClass('is-visible')){
			window.requestAnimationFrame(resizeQuickView);
		}
	});

	function updateSlider(navigation) {
		var sliderConatiner = navigation.parents('.product-slider-wrapper').find('.product-slider'),
			activeSlider = sliderConatiner.children('.selected').removeClass('selected');
		if ( navigation.hasClass('product-next') ) {
			( !activeSlider.is(':last-child') ) ? activeSlider.next().addClass('selected') : sliderConatiner.children('li').eq(0).addClass('selected'); 
		} else {
			( !activeSlider.is(':first-child') ) ? activeSlider.prev().addClass('selected') : sliderConatiner.children('li').last().addClass('selected');
		} 
	}

	function updateQuickView(url,selectProduct) {
		$(selectProduct+'.product-quick-view '+selectProduct+'.product-slider li').removeClass('selected').find('img[src="'+ url +'"]').parent('li').addClass('selected');
	}

	function resizeQuickView(selectProduct) {
		var quickViewLeft = ($(window).width() - $(selectProduct+'.product-quick-view').width())/2,
			quickViewTop = ($(window).height() - $(selectProduct+'.product-quick-view').height())/2;
		$(selectProduct+'.product-quick-view').css({
		    "top": quickViewTop,
		    "left": quickViewLeft
		});
	} 

	function closeQuickView(finalWidth, maxQuickWidth) {
		var close = $('.flag.product-close'),
			selectedProduct=close.attr('href'),
			activeSliderUrl = close.siblings('.product-slider-wrapper').find('.selected img').attr('src'),
			selectedImage = $('.empty-box').find('img');
		//update the image in the gallery
		if( !$(selectedProduct+'.product-quick-view').hasClass('velocity-animating') && $(selectedProduct+'.product-quick-view').hasClass('add-content')) {
			selectedImage.attr('src', activeSliderUrl);
			animateQuickView(selectedImage, finalWidth, maxQuickWidth, 'close',selectedProduct);
		} else {
			closeNoAnimation(selectedImage, finalWidth, maxQuickWidth,selectedProduct);
		}
		close.removeClass('flag');
	}

	function animateQuickView(image, finalWidth, maxQuickWidth, animationType,selectProduct) {
		//store some image data (width, top position, ...)
		//store window data to calculate quick view panel position
		var parentListItem = image.parent('.product-item'),
			topSelected = image.offset().top - $(window).scrollTop(),
			leftSelected = image.offset().left,
			widthSelected = image.width(),
			heightSelected = image.height(),
			windowWidth = $(window).width(),
			windowHeight = $(window).height(),
			finalLeft = (windowWidth - finalWidth)/2,
			finalHeight = finalWidth * heightSelected/widthSelected,
			finalTop = (windowHeight - finalHeight)/2,
			quickViewWidth = ( windowWidth * .8 < maxQuickWidth ) ? windowWidth * .8 : maxQuickWidth ,
			quickViewLeft = (windowWidth - quickViewWidth)/2;
			parentListItem.find('.product-close').addClass('flag');

		if( animationType == 'open') {
			//hide the image in the gallery
			parentListItem.addClass('empty-box');
			//place the quick view over the image gallery and give it the dimension of the gallery image
			$(selectProduct+'.product-quick-view').css({
			    "top": topSelected,
			    "left": leftSelected,
			    "width": widthSelected
			}).velocity({
				//animate the quick view: animate its width and center it in the viewport
				//during this animation, only the slider image is visible
			    'top': finalTop+ 'px',
			    'left': finalLeft+'px',
			    'width': finalWidth+'px'
			}, 1000, [ 400, 20 ], function(){
				//animate the quick view: animate its width to the final value
				$(selectProduct+'.product-quick-view').addClass('animate-width').velocity({
					'left': quickViewLeft+'px',
			    	'width': quickViewWidth+'px'
				}, 300, 'ease' ,function(){
					//show quick view content
					$(selectProduct+'.product-quick-view').addClass('add-content');
				});
			}).addClass('is-visible');
		} else {
			//close the quick view reverting the animation
			$(selectProduct+'.product-quick-view').removeClass('add-content').velocity({
			    'top': finalTop+ 'px',
			    'left': finalLeft+'px',
			    'width': finalWidth+'px'
			}, 300, 'ease', function(){
				$('body').removeClass('overlay-layer');
				$(selectProduct+'.product-quick-view').removeClass('animate-width').velocity({
					"top": topSelected,
				    "left": leftSelected,
				    "width": widthSelected
				}, 500, 'ease', function(){
					$(selectProduct+'.product-quick-view').removeClass('is-visible');
					parentListItem.removeClass('empty-box');
				});
			});
		}
	}
	function closeNoAnimation(image, finalWidth, maxQuickWidth,selectedProduct) {
		var parentListItem = image.parent('.product-item'),
			topSelected = image.offset().top - $(window).scrollTop(),
			leftSelected = image.offset().left,
			widthSelected = image.width();

		//close the quick view reverting the animation
		$('body').removeClass('overlay-layer');
		parentListItem.removeClass('empty-box');
		$(selectedProduct+'.product-quick-view').velocity("stop").removeClass('add-content animate-width is-visible').css({
			"top": topSelected,
		    "left": leftSelected,
		    "width": widthSelected
		});
	}
	$(".add-to-cart").on('click',function(e){
		e.preventDefault();
		var $loader=$(this).find('.fa-spinner');
		$loader.show();
		var $product_id=$(this).data('product-id');
		console.log($product_id);
		$.ajax({
			type:    'post',
			url:     '/addToCart',
			data:     { product_id: $product_id,_token: $("meta[name='csrf-token']").attr('content') },
			datatype: 'json',
			success: function(result){
				$loader.hide();
				console.log(result);
			}
		});

	});
});