var gulp = require('gulp');
var elixir = require('laravel-elixir');
var del = require('del'); // execute: $ npm install --save-dev del

/*
 |----------------------------------------------------------------
 | Have a Drink!
 |----------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic
 | Gulp tasks for your Laravel application. Elixir supports
 | several common CSS, JavaScript and even testing tools!
 |
 */


elixir.extend("remove", function(path) {
    gulp.task("remove", function() {
        del(path);
    });
    return this.queueTask("remove");
});


elixir(function(mix) {
    mix.sass('app.scss')
       .copy(
            'bower_components/jquery/dist/jquery.min.js',
            'public/js/vendor/jquery.js'
        )
       .copy(
            'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
            'public/js/vendor/bootstrap.js'
        )
       .copy(
            'bower_components/font-awesome/css/font-awesome.min.css',
            'public/css/vendor/font-awesome.css'
        )
        .copy(
            'bower_components/font-awesome',
            'public/css/fonts'
        )
        .copy(
            'bower_components/bourbon/app/assets/stylesheets',
            'resources/assets/sass/bourbon'
        )
        .remove([ 'public/css/fonts/less' ]);
});
