<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Application Title -->
	<title>eshop.com - {{ $title or "Welcome to eshop.com" }}</title>

	<!-- Bootstrap CSS -->
	<link href="/css/vendor/reset.css" rel="stylesheet">
	<link href="/css/vendor/font-awesome.css" rel="stylesheet">
	<link href="/css/app.css" rel="stylesheet">

	<!-- Web Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Eshop.com</a>
		</div>

		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				@if(isset($categories))
					@foreach($categories as $category)
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $category->name }} <b class="caret"></b> </a>
							<ul class="dropdown-menu">
							@foreach($category->subcategory as $subcategory)
								<li><a href="/"> {{ $subcategory->name }}</a></li>
							@endforeach
							</ul>
						</li>
					@endforeach
				@endif
			</ul>

			<ul class="nav navbar-nav navbar-right">
				@if (Auth::check())

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="https://www.gravatar.com/avatar/{{{ md5(strtolower(Auth::user()->email)) }}}?s=35" height="35" width="35" class="navbar-avatar">
							{{ Auth::user()->name }} <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="/orders"><i class="fa fa-truck"></i> My Orders</a></li>
							<li><a href="/auth/logout"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
						</ul>
					</li>
				@else
					<li><a href="/auth/login"><i class="fa fa-btn fa-sign-in"></i>Login</a></li>
					<li><a href="/auth/register"><i class="fa fa-btn fa-user"></i>Register</a></li>

				@endif

				<li><a href="/checkout" title="Checkout Cart"><i class="fa fa-lg fa-shopping-cart"></i> <span class="badge" id="cart-counter">{{ (Session::get('cart.totalItem'))}}</span></a></li>
			</ul>
		</div>
	</div>
</nav>

@yield('content')

<!-- JavaScript -->
<script src="/js/vendor/jquery.js"></script>
<script src="/js/vendor/bootstrap.js"></script>
<script src="/js/vendor/modernizr.js"></script>
<script src="/js/vendor/velocity.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>
