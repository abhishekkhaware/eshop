@extends('layouts.default')

@section('content')
<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">List Of Orders</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Order Id#</th>
							<th>Product Name</th>
							<th>Quantity</th>
							<th>Paid Amount</th>
							<th>Status</th>
						</tr>
					</thead>
					@if(isset($orders->id))
					<tbody>
					@foreach($orders as $order)
						<tr>
							<td>{{ $order->id }}</td>
							<td>{{ $order->product->name }}</td>
							<td>{{ $order->quantity }}</td>
							<td>{{ $order->price }}</td>
							<td>{{ $order->status }}</td>
						</tr>
					@endforeach
					</tbody>
						@else
						<tr>
							<td colspan="5" style="text-align: center;font-size:20px;">No Order Found</td>
						</tr>
						@endif
				</table>
				<ul class="pagination pull-right">
					{!! $orders->render() !!}
				</ul>

			</div>
		</div>
	</div>
</div>
@stop
