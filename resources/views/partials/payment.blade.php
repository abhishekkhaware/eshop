@include('partials.errors.basic')
<form class="form-horizontal" role="form" method="POST" action="/auth/login">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label for="email" class="col-sm-3 control-label">Name on card</label>
        <div class="col-sm-6">
            <input type="text" id="card-holder" name="card-holder" class="form-control" placeholder="Name on Card" autocapitalize="off" value="{{ old('email') }}">
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Credit Card</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" placeholder="Credit Card Number">
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">CVC</label>
        <div class="col-sm-3">
            <input type="text" class="form-control input-sm" placeholder="CVC Number">
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Expiration Month</label>
        <div class="col-sm-6">
            <select>
                @foreach(range(1,12) as $month)
                    <option value="{{ $month }}"> {{$month}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Expiration Year</label>
        <div class="col-sm-6">
            <select>
                @foreach(range(2014,2023) as $year)
                    <option value="{{ $year }}"> {{$year}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <blockquote class="blockquote-reverse">
        <span><strong>Powered By: <i class="fa fa-cc-stripe"></i> </strong></span>
    </blockquote>
</form>
