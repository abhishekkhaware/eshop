@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <span class="pull-left"><i class="fa fa-list"></i> List of all Products</span>
                    <form class="navbar-form pull-right " role="search">
                        <div class="clearfix">
                            <input type="text" class="input-sm pull-left form-control" placeholder="Search">
                            <button type="submit" class="btn pull-right btn-sm btn-default"><i class="fa fa-search"></i> </button>
                        </div>
                    </form>
                </div>
                <div class="panel-body">
                    <ul class="product-items product-container">
                        <?php $i=1; ?>
                        @foreach($products as $product)
                            <li class="product-item">
                                <img src="/img/item-{{ rand(1,3) }}.jpg" alt="Item Preview">
                                <a href="#product-quick-view-{{$product->id }}" data-product="{{$product->id }}" class="product-trigger pull-left">Quick View</a>
                                <a href="#" data-product-id="{{$product->id }}" class="add-to-cart add-to-cart-label pull-right"><i class="fa fa-spin fa-spinner" style="display: none"></i> Add to cart</a>
                                <div class="product-quick-view" id="product-quick-view-{{$product->id }}" >
                                    <div class="product-slider-wrapper">
                                        <ul class="product-slider">
                                            <li class="selected"><img src="/img/item-1.jpg" alt="Product Image 1"></li>
                                            <li><img src="/img/item-2.jpg" alt="Product Image 2"></li>
                                            <li><img src="/img/item-3.jpg" alt="Product Image 3"></li>
                                        </ul> <!-- product-slider -->

                                        <ul class="product-slider-navigation">
                                            <li><a class="product-next" href="#0">Prev</a></li>
                                            <li><a class="product-prev" href="#0">Next</a></li>
                                        </ul> <!-- product-slider-navigation -->
                                    </div> <!-- product-slider-wrapper -->

                                    <div class="product-item-info">
                                        <h2>{{ $product->name }}</h2>
                                        <p>{{ $product->description }}</p>
                                        <ul class="product-item-action">
                                            <li><button data-product-id="{{$product->id}}" class="add-to-cart"><i class="fa fa-spin fa-spinner"></i> Add to cart</button></li>
                                            <li><a href="/checkout" class="btn btn-success checkout"> <i class="hide fa fa-shopping-cart"></i> Checkout</a></li>
                                        </ul> <!-- product-item-action -->
                                    </div> <!-- product-item-info -->
                                    <a href="#product-quick-view-{{$product->id }}" class="product-close">Close</a>
                                </div> <!-- product-quick-view -->
                            </li> <!-- product-item -->
                        @endforeach
                    </ul> <!-- product-items -->

                    <ul class="pagination pull-right">
                        {!! $products->render() !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

