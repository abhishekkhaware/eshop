@extends('layouts.default')

@section('content')
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Selected Products In Cart (Checkout Process)</div>
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Sl#</th>
							<th>Product Name</th>
							<th>Quantity</th>
							<th>Payable Amount($)</th>
							<th>Action</th>
						</tr>
						</thead>
						@if(count($carts))
							<tbody>
							<?php $i=1;$totalPrice=0; ?>
							@foreach($carts as $cart)
								<tr>
									<td>{{ $i++ }}</td>
									<td nowrap>{{ $cart->product_id }}</td>
									<td>
										<input type="text" class="input-sm" value="{{ $q=$cart->quantity  }}"/>
									</td>
									<td>
										<i class="fa fa-dollar"></i> {{ $p=($cart->price) }}

										<input type="hidden" value="{{ $totalPrice= $totalPrice + ($q * $p)}}"/>
									</td>
									<td nowrap>
										<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Update</a>
										<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
							@endforeach
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
								<td>
									<strong>Total Payable Amount:</strong>
								</td>
								<td nowrap><strong> $ {{ $totalPrice }} </strong></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
								<td><button class="btn btn-lg btn-success" data-toggle="modal" data-target="#payment-box"><i class="fa fa-cc-stripe"></i> &nbsp; Checkout $ {{ $totalPrice }} </button></td>
							</tr>
							</tbody>
						@else
							<tr>
								<td colspan="5" style="text-align: center;font-size:20px;">Cart Is Empty</td>
							</tr>
						@endif
					</table>
					<ul class="pagination pull-right">
					{!! $carts->render() !!}
					</ul>

					<div class="modal fade" id="payment-box">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"> <i class="fa fa-times"></i></span></button>
									<h4 class="modal-title">Payment Form </h4>
								</div>
								<div class="modal-body">
									@include('partials.payment')
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary"><i class="fa fa-credit-card"></i> Pay </button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>
			</div>
		</div>
	</div>
@stop
