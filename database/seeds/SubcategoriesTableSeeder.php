<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Subcategory;

class SubcategoriesTableSeeder extends Seeder {

	public function run()
	{
//		$faker = Faker::create();
		$sub_cats=[['T-Shirt','Jeans','Trousers','Winter Wear','Formal Shirts','Belts','Wallets','Casual Shoes','Sport Shoes','Boots'],
			['Skirts','Jeans And Shorts','Sarees','Heels','Winter Wears','Handbags','Perfumes','Watches'],
			['T-Shirts','Jeans','Shirts','Tees And Tops','Diapers','Wipers','Soft Toys','Educational Toys','Puzzles'],
			['Nexus 6','Moto G', 'Moto X','Iphone','Iphone6','HTC Desire','HTC Desire 820'],
			['Dell','Apple','Lenovo', 'Compaq','HP'],
			['Literature','Fiction','Academic And Professional','Business and Management'],
			['Formal Shoes','Casual Shoes','Sport Shoes'],['Chess','Darts','Basketball','Cricket','Football','Tennis','Golf'],
			['Bed','Blankets','Mattresses','Mats And Carpets'],['Sony Playstation','Microsoft Xbox','PC Games','PS3','PS4']];

		foreach(range(0, 9) as $index)
		{
			foreach($sub_cats[$index] as $sub_cat)
			Subcategory::create([
				'category_id'=>$index+1,
				'name'=>$sub_cat
			]);
		}
	}

}
