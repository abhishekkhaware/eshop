<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
//		$faker = Faker::create();
		$cat=['Men','Women','Kids','Mobiles','Laptops','Books','Shoes','Sports', 'Home Furnishing','Gaming'];

		foreach(range(1, 10) as $index)
		{
			Category::create([
				'name'=> $cat[$index-1]
			]);
		}
	}

}
