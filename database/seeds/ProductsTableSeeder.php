<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 1500) as $index)
		{
			Product::create(['name'=>$faker->name,
				'description'=> $faker->realText($faker->numberBetween(40,150)),
				'category_id'=>$faker->randomDigitNotNull,
				'subcategory_id'=>$faker->randomDigitNotNull,
				'tag'=>$faker->sentence($faker->randomDigitNotNull),
				'slug'=>$faker->slug(),
				'stock'=>$faker->randomNumber(3),
				'price'=> $faker->randomNumber(2)

			]);
		}
	}

}
