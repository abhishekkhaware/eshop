<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => getenv('MAILGUN_DOMAIN'),
		'secret' => getenv('MAILGUN_SECRET_KEY'),
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => getenv('STRIPE_SECRET_KEY'),
	],
	'google' => [
		'client_id'  => getenv('GOOGLE_CLIENT_ID'),
		'client_secret'  => getenv('GOOGLE_CLIENT_SECRET'),
		'redirect' => getenv('GOOGLE_OAUTH_REDIRECT'),
	],


];
