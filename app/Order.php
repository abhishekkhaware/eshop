<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

	protected $fillable = [];
	public function product()
	{
		return $this->hasMany('App\Product');
	}

}
