<?php namespace App\Http\Controllers;

use App\Category;
use App\Product;

class WelcomeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products= Product::paginate(10);
		$categories= Category::with('subcategory')->take(8)->get();
                          //return dd($categories);
		return view('products.index',compact('products','categories'));
	}

}
