<?php namespace App\Http\Controllers;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartRequest;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartsController extends Controller {
	public $totalItem;
	public static $tax=0.2;
	public $payableAmount;

	function __construct()
	{
		$this->totalItem=0;
		$this->payableAmount=0;
	}
	// Add item to cart
	public function add(CartRequest $request ){

		try{
			$productId=$request->get('product_id');
			$sessionId=Session::getId();

			$product = Product::find($productId)->first();

			if($product->exists){

				$myCart=DB::table('carts')->where(['session_id'=>$sessionId,'product_id'=>$productId])->first();

				if(!is_null($myCart))
				{
					DB::table('carts')->where(['session_id'=>$sessionId,'product_id'=>$productId])
						->update(['price'=> DB::raw('price + '. $product->price),
								'quantity'=>DB::raw('quantity + 1'),
								'updated_at'=> Carbon::now()
						]);

				}else{

					DB::table('carts')->insert(['product_id'=>$productId,
						'session_id'=> $sessionId,
						'price'=> $product->price,
						'quantity'=> 1,
						'created_at'=> Carbon::now()

					]);
				}

				$myCart=DB::table('carts')->select(DB::raw('count(*) as totalItem, sum(quantity * price) as totalAmount'))->where(['session_id'=>$sessionId])->first();

				Session::put('cart.totalItem',$myCart->totalItem);
				Session::put('cart.totalAmount',$myCart->totalAmount);
				return response()->json(['status'=>'success','message'=>"Cart has been updated!",
					'totalItem'=>$myCart->totalItem,
					'totalAmount'=>$myCart->totalAmount
				]);
			}

		}catch (\Exception $e){
			return response('Failed!'.$e->getMessage(),301);
		}
		return response('Ops! Something Went Wrong! Please Try again later!');
	}
	// get total items in cart
	public function totalItems()
	{
		return $myCart=Cart::where('session_id',Session::getId());
	}
	// get total payable amount
	public function totalPrice()
	{
		return $this->payableAmount * self::$tax;

	}

}
