<?php namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;

class HomeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products= Product::paginate(10);
		$categories= Category::with('subcategory')->take(8)->get();
		return view('products.index',compact('products','categories'));
	}

}
